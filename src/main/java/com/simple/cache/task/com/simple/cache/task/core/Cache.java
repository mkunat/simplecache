/**
 * 
 */
package com.simple.cache.task.com.simple.cache.task.core;

/**
 * @author Magda
 *
 */
public interface Cache<K,V> {
	
	/**
	 * Returns the value associated with key in the cache
	 * 
	 * @param key Cache entry key
	 * @return value associated with key
	 * @throws IllegalArgumentException when key is null
	 */
	V get(final K key) throws IllegalArgumentException;
	
	/**
	 * Stores a cache entry. Associate value with key in this cache.
	 * 
	 * @param key  Cache entry key
	 * @param value Cache entry value
	 */
	void put(final K key, final V value);
	
	/**
	 * Removes a cache entry associated with the key 
	 * 
	 * @param key Cache entry key
	 */
	void delete(final K key);
	
	/**
	 * Removes all entries from cache 
	 * 
	 */
	void invalidateAll();

}
