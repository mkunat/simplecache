package com.simple.cache.task.com.simple.cache.task.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.codahale.metrics.annotation.Timed;
import com.simple.cache.task.com.simple.cache.task.core.CacheObject;
import com.simple.cache.task.com.simple.cache.task.core.SimpleCache;

@Path("/cache")
public class CacheService {

	private final SimpleCache cache;
	
	public CacheService(SimpleCache cache) {
		this.cache = cache;
	}
	
    @GET
    @Timed
    @Path("/getObject/{key}")
    @Produces(MediaType.APPLICATION_JSON)
    public CacheObject getObject(@PathParam("key") String key) {
        return (CacheObject) cache.getObject(key);
    }
 
    @GET
    @Timed
    @Path("/get/{key}")
    @Produces(MediaType.APPLICATION_JSON)
    public String get(@PathParam("key") String key) {
        return cache.get(key);
    }
    
    @DELETE
    @Timed
    @Path("/remove/{key}")
    @Produces(MediaType.TEXT_PLAIN)
    public String removeCacheObject(@PathParam("key") String key) {
    	cache.delete(key);
        return "Cache element removed";
    }
 
    @DELETE
    @Timed
    @Path("/invalidateAll")
    @Produces(MediaType.APPLICATION_JSON)
    public String invalidateAll() {
    	cache.invalidateAll();
        return "All elements in cache are discarded";
    }
 
    @POST
    @Timed
    @Path("/add")
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes({MediaType.APPLICATION_JSON})
    public String addCacheObject(CacheObject obj) {
    	cache.put(obj.getKey(),obj.getValue());
        return "Element added successfully";
    }
}
