package com.simple.cache.task.com.simple.cache.task;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import com.simple.cache.task.com.simple.cache.task.core.SimpleCache;
import com.simple.cache.task.com.simple.cache.task.resources.CacheService;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

/**
 * Simple cache application that stores objects in memory. 
 * It Automatically removes expired elements from cache.
 * The cleanup task is run every 3 minutes,
 * A cache object is considered as expired after a minute
 * 
 * @author MagdaK-C
 *
 */
public class SimpleCacheTaskApplication extends Application<SimpleCacheTaskConfiguration> {

    public static void main(final String[] args) throws Exception {
        new SimpleCacheTaskApplication().run(args);
    }

    @Override
    public String getName() {
        return "SimpleCacheTask";
    }

    @Override
    public void initialize(final Bootstrap<SimpleCacheTaskConfiguration> bootstrap) {
        // TODO: application initialization
    }

    @Override
    public void run(final SimpleCacheTaskConfiguration configuration,
                    final Environment environment) {
    	final SimpleCache cache = new SimpleCache(Duration.ofMinutes(1),3,TimeUnit.MINUTES);
        environment.jersey().register(new CacheService(cache));
    }

}
