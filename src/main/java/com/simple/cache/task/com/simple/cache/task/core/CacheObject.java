package com.simple.cache.task.com.simple.cache.task.core;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CacheObject {
	private long lastUsed = System.currentTimeMillis();
	private String key;
	private String value;

	public CacheObject() {
		// Needed by Jackson deserialization
	}
	
	public CacheObject(String key, String value) {
		this.key = key;
		this.value = value;
	}
	
	@JsonProperty
	public String getKey() {
		return key;
	}
	
	@JsonProperty
	public String getValue() {
		return value;
	}
	
	protected void setLastUsed(long lastUsed) {
		this.lastUsed = lastUsed;
	}
	
	@JsonProperty
	public long getlastUsed() {
		return lastUsed;
	}
	
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		CacheObject other = (CacheObject) obj;
		return key.equals(other.key) && value.equals(other.value);
	}
	
	public int hashCode() {
		return Objects.hash(key, value);
	}
}
