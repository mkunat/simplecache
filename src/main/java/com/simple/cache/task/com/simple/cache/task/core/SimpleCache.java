/**
 * 
 */
package com.simple.cache.task.com.simple.cache.task.core;

import java.time.Duration;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Simple cache stores objects in a map in memory. Automatically removes expired elements from cache.
 * @author MagdaK-C
 *
 */
public class SimpleCache implements Cache<String,String> {

	private final Map<String, CacheObject> cacheMap = new ConcurrentHashMap<String, CacheObject>();

	private final Duration lifeTime;
	
	private long period = 3;
	
	private TimeUnit unit = TimeUnit.MINUTES;

	private static final ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();

	/**
	 * Create a simple cache object
	 * @param lifeTime Time after which object cache is expired
	 * @param period Time period how often the cleanup task should be executed. If number is lower then 0 then the default number 3 is taken
	 * @param unit Time unit interval of the cleanup task. When null TimeUnit.MINUTES is the default
	 */
	public SimpleCache(final Duration lifeTime, long period, TimeUnit unit) {
		this.lifeTime = lifeTime;
		if (period > 0 ) {
			this.period = period;
		}
		if ( unit != null) {
			this.unit = unit;
		}
		service.scheduleAtFixedRate(new CleanupTask(), lifeTime.toMillis(), this.period, this.unit);
	}

	/*
	 * Gets the whole cache entry object associated with the key
	 */
	public CacheObject getObject(String key) {
		CacheObject cacheObject = (CacheObject) cacheMap.get(key);
		if (cacheObject == null) {
			return null;
		} else {
			cacheObject.setLastUsed(System.currentTimeMillis());
			return cacheObject;
		}
	}

	@Override
	public String get(String key) throws IllegalArgumentException {

		if (key == null) throw new IllegalArgumentException("Cache key can not be null");

		CacheObject cacheObject = (CacheObject) cacheMap.get(key);
		if (cacheObject == null) {
			return null;
		} else {
			cacheObject.setLastUsed(System.currentTimeMillis());
			return cacheObject.getValue();
		}
	}
	
	@Override
	public void put(String key, String value) throws IllegalArgumentException {
		
		if (key == null) throw new IllegalArgumentException("Cache key can not be null");
		
		cacheMap.putIfAbsent(key, new CacheObject(key,value));
	}

	@Override
	public void delete(String key) {
		if (key == null) throw new IllegalArgumentException("Cache key can not be null");
		cacheMap.remove(key);
	}

	@Override
	public void invalidateAll() {
		cacheMap.clear();
	}

	private class CleanupTask implements Runnable {
		public void run() {
			long timeNow = System.currentTimeMillis();
			long lifeTimeMilis = lifeTime.toMillis();
			cacheMap.entrySet()
					.removeIf(e -> {
						CacheObject obj = e.getValue();
						return (obj != null) && (timeNow > lifeTimeMilis + obj.getlastUsed());
					});
		}
	}
}
