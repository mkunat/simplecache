package com.simple.cache.task.com.simple.cache.task.resources;

import static org.junit.Assert.*;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Test;

import com.simple.cache.task.com.simple.cache.task.core.CacheObject;
import com.simple.cache.task.com.simple.cache.task.core.SimpleCache;

public class CacheServiceTest {

	CacheService service;
	
	@Before
	public void setUpBefore() throws Exception {
		SimpleCache cache = new SimpleCache(Duration.ofMinutes(1), 2, TimeUnit.MINUTES);
		cache.put("1", "string1");
		cache.put("2", "string2");
		cache.put("3", "string3");
		service = new CacheService(cache);
	}

	@Test
	public void testGetObject() {
		assertEquals(new CacheObject("1", "string1"), service.getObject("1"));
		assertEquals(new CacheObject("2", "string2"), service.getObject("2"));
	}

	@Test
	public void testGet() {
		assertEquals("string1", service.get("1"));
		assertEquals("string2", service.get("2"));
	}

	@Test
	public void testRemoveCacheObject() {
		service.removeCacheObject("1");
		assertNull(service.get("1"));
	}

	@Test
	public void testInvalidateAll() {
		service.invalidateAll();
		assertNull(service.get("1"));
		assertNull(service.get("2"));
		assertNull(service.get("3"));
	}

	@Test
	public void testAddCacheObject() {
		service.addCacheObject(new CacheObject("key","value"));
		assertEquals("value", service.get("key"));
	}

}
