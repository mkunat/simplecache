package com.simple.cache.task.com.simple.cache.task.core;

import static org.junit.Assert.*;

import java.time.Duration;
import java.util.concurrent.TimeUnit;
import org.junit.Test;

public class SimpleCacheTest {
	
	@Test
	public void testGet() {
		SimpleCache cache = new SimpleCache(Duration.ofMinutes(1), 2, TimeUnit.MINUTES);
		cache.put("key", "value");
		assertEquals("value", cache.get("key"));
		//null for not existing key
		assertNull(cache.get("key1"));
		
	}
	
	@Test
	public void testGetObject() throws InterruptedException {
		SimpleCache cache = new SimpleCache(Duration.ofMinutes(1), 2, TimeUnit.MINUTES);
		cache.put("key", "value");
		long lastUsed = cache.getObject("key").getlastUsed();
		assertEquals(new CacheObject("key","value"), cache.getObject("key"));
		Thread.sleep(200);
		//last used field is updated
		assertNotEquals(lastUsed, cache.getObject("key").getlastUsed());
	}
	
	@Test(expected = java.lang.IllegalArgumentException.class)  
	public void testGetWithNullKey() throws IllegalArgumentException {
		SimpleCache cache = new SimpleCache(Duration.ofMinutes(1), 2, TimeUnit.MINUTES);
		cache.get(null);
	}

	@Test
	public void testPut() {
		SimpleCache cache = new SimpleCache(Duration.ofMinutes(1), 2, TimeUnit.MINUTES);
		cache.put("1", "test123");
		assertEquals("test123", cache.get("1"));
		
		cache.put("key", null);
		assertEquals(null, cache.get("key"));
		
		cache.put("ółę&#", "ółę&#");
		assertEquals("ółę&#", cache.get("ółę&#"));
		
	}
	
	@Test
	public void testPutWithTheSameKey() {
		SimpleCache cache = new SimpleCache(Duration.ofMinutes(1), 2, TimeUnit.MINUTES);
		cache.put("1", "test1");
		assertEquals("test1", cache.get("1"));
		//put an element with the same key does not update the value
		cache.put("1", "test2");
		assertEquals("test1", cache.get("1"));
		
	}
	
	@Test(expected = IllegalArgumentException.class)  
	public void testPutWithNullKey() {
		SimpleCache cache = new SimpleCache(Duration.ofMinutes(1), 2, TimeUnit.MINUTES);
		cache.put(null, "test");
	}

	@Test
	public void testDelete() {
		SimpleCache cache = new SimpleCache(Duration.ofMinutes(1), 2, TimeUnit.MINUTES);
		cache.put("1", "test1");
		cache.delete("1");
		assertNull(cache.get("1"));
	}

	@Test(expected = java.lang.IllegalArgumentException.class) 
	public void testDeleteNullKey() {
		SimpleCache cache = new SimpleCache(Duration.ofMinutes(1), 2, TimeUnit.MINUTES);
		cache.delete(null);
	}
	
	@Test
	public void testInvalidateAll() {
		SimpleCache cache = new SimpleCache(Duration.ofMinutes(1), 2, TimeUnit.MINUTES);
		cache.put("1", "test1");
		cache.put("2", "test2");
		cache.put("3", "test3");
		cache.put("4", "test4");
		cache.invalidateAll();
		assertNull(cache.get("1"));
		assertNull(cache.get("2"));
		assertNull(cache.get("3"));
		assertNull(cache.get("4"));
	}

	@Test
	public void testAutomaticalRemoving() throws InterruptedException {
		SimpleCache cache = new SimpleCache(Duration.ofMillis(100), 150, TimeUnit.MILLISECONDS);
		cache.put("1", "test1");
		cache.put("2", "test2");
		cache.put("3", "test3");
		cache.put("4", "test1");
		Thread.sleep(500);
		assertNull(cache.get("1"));
		assertNull(cache.get("2"));
		assertNull(cache.get("3"));
		assertNull(cache.get("4"));
	}
	
	@Test
	public void testNotRemovedWhenNotExpired() throws InterruptedException {
		SimpleCache cache = new SimpleCache(Duration.ofMillis(800), 100, TimeUnit.MILLISECONDS);
		cache.put("1", "entry1");
		cache.put("2", "entry2");
		cache.put("3", "entry3");
		cache.put("4", "entry4");
		Thread.sleep(200);
		assertNotNull(cache.get("1"));
		assertNotNull(cache.get("2"));
		assertNotNull(cache.get("3"));
		assertNotNull(cache.get("4"));
	}
	
	@Test
	public void testZeroLifeTime() throws InterruptedException {
		SimpleCache cache = new SimpleCache(Duration.ZERO, 100, TimeUnit.MILLISECONDS);
		cache.put("1", "entry1");
		cache.put("2", "entry2");
		cache.put("3", "entry3");
		cache.put("4", "entry4");
		Thread.sleep(120);
		assertNull(cache.get("1"));
		assertNull(cache.get("2"));
		assertNull(cache.get("3"));
		assertNull(cache.get("4"));
	}

}
